#!/usr/bin/perl
use strict;
use LWP::Simple;
use File::Basename;

my $event = shift;

my $root  = '/home/crthomas/.pianobar';
my $info  = "$root/nowplaying.txt";
my $up    = $root . '/thumbsup.png';
my $down  = $root . '/thumbsdown.png';
my $noart = $root . '/musicnote.png';

my @events = qw( songstart songban songlove info );

my @cmd = ( 'notify-send', '-t', '3000' );

my $debug = 1 if ( -e '/tmp/pianobar_event.log' );

if ( grep /$event/, @events ) {
	my ( $artist, $title, $album, $rating, $from, $arturl );

	open ( INFO, ">$info" ) if $event ne 'info';
	open ( LOG, '>>/tmp/pianobar_event.log' ) if $debug;

	while ( <STDIN> ) {
		print INFO if $event ne 'info';
		print LOG  if $debug;
		chomp;
		my ( $key, $value ) = split /=/;
		$artist = $value if ( $key eq 'artist'          );
		$title  = $value if ( $key eq 'title'           );
		$album  = $value if ( $key eq 'album'           );
		$rating = $value if ( $key eq 'rating'          );
		$from   = $value if ( $key eq 'songStationName' );
		$arturl = $value if ( $key eq 'coverArt'        );
	}

	my $artfile = _get_cover_art ( $arturl ) if $arturl;
	   $artfile = $noart unless $artfile;

	push ( @cmd, '-i', $up      ) if ( $event eq 'songlove'  && -e $up      );
	push ( @cmd, '-i', $down    ) if ( $event eq 'songban'   && -e $down    );
	push ( @cmd, '-i', $artfile ) if ( $event eq 'songstart' || $event eq 'info' && -e $artfile );

	my $header;
		$header .= $title;
		$header .= " <3"  if ( $event eq 'songstart' || $event eq 'info' ) && $rating == 1;

	my $detail;
		$detail .= "By: $artist";
		$detail .= "\nOn: $album" if $album;
		$detail .= "\nFrom: $from" if $from;

	push ( @cmd, $header, $detail );
	system ( @cmd );

	close INFO if $event ne 'info';
	close LOG  if $debug;
}

sub _get_cover_art {
	my ( $arturl ) = @_;
	mkdir $root unless -e $root;
	my $artfile = $root . basename ( $arturl );
	if ( -e $artfile ) {
		return $artfile;
	} else {
		my $response = getstore ( $arturl, $artfile );
		return $artfile if $response eq 200;
		print LOG "RESPONSE: $response\n" if $debug;
	}
	return;
}
