#!/bin/bash
PBIN='/usr/local/bin/pianobar'
CTLFILE='/home/crthomas/.config/pianobar/ctl'
PAUSELOCK='/tmp/paused'
LOGFILE='/dev/null'
INFOFILE='/home/crthomas/.pianobar/nowplaying.txt'
VOLUMEINCREMENT=5

sendcmd() {
	echo -n "$1" > $CTLFILE
}

rmlock() {
	if [ -e $PAUSELOCK ]; then
		rm $PAUSELOCK
	fi
}

setlock() {
	touch $PAUSELOCK
}

if [ -z $1 ] || [ $1 == 'help' ] || [ $1 == 'h' ]; then
	echo `basename $0`
	echo ---
	echo info - trigger pianobar_eventlistener to display song info
	echo play - start pianobar, or unpause if paused
	echo pause - pause pianobar
	echo stop - quit pianobar
	echo skip - play the next song
	echo tired - don\'t play this song for a while
	echo thumbsup - love current song
	echo thumbsdown - ban current song
	echo louder - turn the volume up
	echo softer - turn the volume down
	echo cron - if screensaver is on, pause
	exit
fi
if [ $1 == 'play' ]; then
	if [ -z $(pidof pianobar) ]; then
		$PBIN	&>$LOGFILE &
	elif [ -e $PAUSELOCK ]; then
		rmlock
		sendcmd 'p'
		pianobar_eventlistener info < $INFOFILE
	fi
elif [ $1 == 'pause' ]; then
	if [ ! -e $PAUSELOCK ]; then
		sendcmd 'p'
		setlock
	fi
elif [ $1 == 'stop' ]; then
	rmlock
	sendcmd 'q'
elif [ $1 == 'skip' ]; then
	sendcmd 'n'
elif [ $1 == 'tired' ]; then
	sendcmd 't'
elif [ $1 == 'thumbsup' ]; then
	sendcmd '+'
elif [ $1 == 'thumbsdown' ]; then
	sendcmd '-'
elif [ $1 == 'cron' ] &&  [ $(pidof -s pianobar) ] && [ ! -e $PAUSELOCK ]; then
	. `dirname $0`/functions/ssactive.sh
	ssactive
	SSRUNNING=$?
	if [ $SSRUNNING -eq 1 ]; then
		setlock
		sendcmd 'p'
	fi
elif [ $1 == 'info' ]; then
	pianobar_eventlistener info < $INFOFILE
elif [ $1 == 'louder' ]; then
	for i in `seq 1 $VOLUMEINCREMENT`; do
		sendcmd ')'
	done
elif [ $1 == 'softer' ]; then
	for i in `seq 1 $VOLUMEINCREMENT`; do
		sendcmd '('
	done
fi
exit
